import Create from '../src/pages/Create';//import Create page
import Read from '../src/pages/./Read';//import Read Page
import Update from '../src/pages/Update';//import Update Page
import Delete from '../src/pages/Delete';//import Delete Page 

import {BrowserRouter as Router,
        Route,
} from "react-router-dom";//import some react router dom

function App() {
  return (
    <Router>
      <Route path="/create" component={Create}/>{/*Route for create page*/}
      <Route path="/Read" component={Read}/>{/*Route for read page*/}
      <Route path="/Update" component={Update}/>{/*Route for update page */}
      <Route path="/Delete" component={Delete}/>{/*Route for delete page */}
    </Router>
  );
}

export default App;
