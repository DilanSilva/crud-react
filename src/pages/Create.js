import React,{useState} from "react";
import NavBar from "../components/NavBar"; //import NavBar

import { Form, Button, Container } from "react-bootstrap"; //import bootstrap

const Create = () => {

  const [heading,setHeading] = useState();//state for heading data
  const [body,setBody] = useState();//state for body data

  const onSubmit = (e) => { //this function getting fired when button is clicked
   
    e.preventDefault(); //prevent the refreshing page
    console.log(heading);

  };

  return (
    <div>
      <NavBar />
      <Container>
        <br />
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Enter The Headings</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Enter Text Here"
              onChange={(e) => {setHeading(e.value.target)}} 
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>
              Enter The Body Here<small class="text-muted"></small>
            </Form.Label>
            <Form.Control 
              as="textarea" 
              placeholder="Enter Text Here" 
            />
          </Form.Group>

          <Button variant="primary" type="submit" onClick={onSubmit}>
            Create
          </Button>
        </Form>
         
      </Container>
    </div>
  );
};

export default Create;
