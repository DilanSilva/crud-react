import React,{useState} from "react";
import { Card, 
         Button, 
         Jumbotron } from "react-bootstrap";

import NavBar from "../components/NavBar"; //import navbar

const Delete = () => {
    const [details,setDetails] = useState([
        {title : "This is Header 1",body : "This is Body 1"},
        {title : "This is Header 2",body : "This is Body 2"},
        {title : "This is Header 3",body : "This is Body 3"},
        {title : "This is Header 4",body : "This is Body 4"},
        {title : "This is Header 5",body : "This is Body 5"},
        {title : "This is Header 6",body : "This is Body 6"}
    ]);
  return (
    <div>
      <NavBar />
        {
            details.map((data) => {
                return(
                    <DeleteCard
                        title={data.title}
                        body={data.body}
                    />
                );
            })
        }
    </div>
  );
};

const DeleteCard = (props) => {
  return (
    <Jumbotron>
      <Card>
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Body>{props.body}</Card.Body>
          <Button variant="danger">Delete</Button>
        </Card.Body>
      </Card>
    </Jumbotron>
  );
};

export default Delete;
