import React,{useState} from "react";
import NavBar from "../components/NavBar"; //import NavBar
import UpdateCard from "../components/UpdateCard";//import Updatecard

const Update = () => {
  const [details,setDetails] = useState([
    {title : "This is Header 1",body : "This is Body 1"},
    {title : "This is Header 2",body : "This is Body 2"},
    {title : "This is Header 3",body : "This is Body 3"},
    {title : "This is Header 4",body : "This is Body 4"},
    {title : "This is Header 5",body : "This is Body 5"},
    {title : "This is Header 6",body : "This is Body 6"}
]);
  return (
    <div>
      <NavBar />
        {
          details.map((data) => {
            return (
              <UpdateCard
                title={data.title}
                body={data.body}
              />
            );
          })
        }
    </div>
  );
};

export default Update;
