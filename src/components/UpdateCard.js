import React from "react";
import { Container, 
         Button,
         Form} from "react-bootstrap";

const UpdateCard = (props) => {
  return (
    <div><br/>
      <Container>
            <br/>
        <Form>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>Enter The Heading</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Enter Text Here"
              value={props.title}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Enter The Body Here<small class="text-muted"></small></Form.Label>
            <Form.Control 
              as="textarea" 
              placeholder="Enter Text Here"
              value={props.body} 
            />
          </Form.Group>
         
          <Button 
            variant="warning" 
            type="submit"
          >
            update
          </Button>

        </Form>
      </Container>
    </div>
  );
};

export default UpdateCard;
