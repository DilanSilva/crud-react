import React from "react";

import { Card, Container } from "react-bootstrap"; //import bootstrap

const CardScreen = (props) => {
  return (
    <div>
      <br />
      <Container>
        <Card>
          <Card.Body>
            <Card.Title>{props.title}</Card.Title>
            <Card.Text>{props.body}</Card.Text>
          </Card.Body>
        </Card>
      </Container>
    </div>
  );
};

export default CardScreen;
